package com.spring.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
/**
 * @version 1
 * @author sanda
 * La classe principale qui met active les web-services
 */
public class CarApplication {

    public static void main(String[] args) {
            SpringApplication.run(CarApplication.class, args);
    }

}
