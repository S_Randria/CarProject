package com.spring.car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author sanda
 * L'interface JPA qui accède à la base grâce à la dépendance data-jpa de Maven
 * Les fonctions employées sont : findAll - save pour la classe CarEntity
 */
@Repository
public interface CarRepository extends JpaRepository<CarEntity, String> {
    
}
