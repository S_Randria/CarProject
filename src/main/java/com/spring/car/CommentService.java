/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.car;

import java.util.List;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sanda
 */
@Service
public class CommentService {
    
    /**
     * accès aux données
     */
    private CommentRepository commentRepository ;
    
    /**
     * constructeur unique
     * @param commentRepository pour l'accès aux données
     * accessible uniquement dans les classes de son package
     */
    protected CommentService(CommentRepository commentRepository) {
        this.setCommentRepository(commentRepository);
    }

    /**
     * getter de l'attribut commentRepository
     * @return CommentRepository
     * accessible uniquement dans cette classe service
     */
    private CommentRepository getCommentRepository() {
        return commentRepository;
    }

    /**
     * setter de l'attribut commentRepository
     * @return CommentRepository
     * accessible uniquement dans cette classe service
     */
    private void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }
    
    /**
     * @param idUser
     * @param idCar
     * @param comment
     * @return l'instance de la voiture inserée
     */
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public CommentEntity save(String idUser,String idCar,String comment) {
        CommentEntity carTemplate = new CommentEntity(idUser,idCar,comment);
        CommentEntity savedComment = this.getCommentRepository().save(carTemplate);
        return savedComment;
    }
    
    /**
     * @param idCar
     * @return la liste des commentaires
     */
    public List<CommentEntity> getComments(String idCar){
        CommentEntity commentCar = new CommentEntity(idCar);
        Example referenceCar = Example.of(commentCar);
        List<CommentEntity> list = this.getCommentRepository().findAll(referenceCar);
        return list;
    }
}
