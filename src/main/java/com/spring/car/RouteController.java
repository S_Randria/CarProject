/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.car;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sanda
 */
@RestController
@RequestMapping("/api/route")
public class RouteController {
    
    @Autowired
    private CarRepository carRepository ;
    
    @GetMapping("/")
    public String doIt(){
        return "hello route";
    }
    @GetMapping("/get-all")
    public List<CarEntity> getAll(){
        List<CarEntity> allCarlist = carRepository.findAll();
        return allCarlist;
    }
    
    
    @PostMapping("/create/{marque}")
    public CarEntity create(@PathVariable String marque) {
        CarEntity carTemplate = new CarEntity(marque);
        CarEntity savedCar = carRepository.save(carTemplate);
        return savedCar;
    }
}
